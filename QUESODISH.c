#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

struct particle {
   long ID;
   double x;
   double y;
   double z;
   double vx;
   double vy;
   double vz;   
};

struct vector {
  double x;
  double y;
  double z;
};

struct vector a(double x, double y, double z, double GM_over_a2_plummer) //conservative force (well, acceleration of the plummer)
{ //x, y, z, are expressed in units of a_plummer
    struct vector acceleration;
    double r2 = x*x + y*y + z*z;
	double coefficient = -GM_over_a2_plummer*(1.0/pow(1.0+r2,1.5));
    acceleration.x = coefficient*x;
    acceleration.y = coefficient*y;
    acceleration.z = coefficient*z;
    return(acceleration);
}

struct particle diskShock(struct particle p, double intensity)
{   //there is no loss in generality taking the normal to the disk on the (Y,Z) plane
    double alpha = 0.25*M_PI; //angle from Omega (usually Omega is along z axis) to normal of disk
    double u = p.z*cos(alpha) - p.y*sin(alpha);
    p.vz -= intensity*cos(alpha)*u;
    p.vy +=  intensity*sin(alpha)*u;    
    return(p);
}

struct particle onestep(struct particle p, double GM_over_a2_plummer, double dt, double Omega)
{
    struct vector acceleration = a(p.x, p.y, p.z, GM_over_a2_plummer);
    double vx_n_14 = p.vx - 0.5*dt*(Omega*Omega*p.x - acceleration.x);
    double P_n_y = p.vy + 2.0*Omega*p.x + 0.5*dt*acceleration.y;
    double vx_n_12 = vx_n_14 + dt*Omega*P_n_y;
    double vy_n_12 = P_n_y - Omega*p.x - Omega*(p.x + dt*vx_n_12);
    double vz_n_12 = p.vz + 0.5*dt*(-Omega*Omega*p.z + acceleration.z);

    p.x = p.x + dt*vx_n_12;
    p.y = p.y + dt*vy_n_12;
    p.z = p.z + dt*vz_n_12;

    acceleration = a(p.x, p.y, p.z, GM_over_a2_plummer);
    double vx_n_34 = vx_n_12 + dt*Omega*P_n_y;
    double vx_n_1 = vx_n_34 - 0.5*dt*(Omega*Omega*p.x - acceleration.x);
    double vy_n_1 = P_n_y - 2.0*Omega*p.x + 0.5*dt*acceleration.y;
    double vz_n_1 = vz_n_12 + 0.5*dt*(-Omega*Omega*p.z + acceleration.z);

    p.vx = vx_n_1;
    p.vy = vy_n_1;
    p.vz = vz_n_1;

    return(p);
}

double r()
{
    return (double)rand()/(double)(RAND_MAX);
}

struct vector randomvector(double v)
{
    struct vector u;
    double phi = 2.0*M_PI*r();
    double theta = acos(-1.0 + 2.0*r());
    u.x = v*sin(theta)*sin(phi);
    u.y = v*sin(theta)*cos(phi);
    u.z = v*cos(theta);
    return u;
}

struct particle newparticle(long ID)
{
    // Create a new particle with the given ID
    struct particle p;
    p.ID = ID; 
    // Initialize its position extracting from Plummer distribution  
    double rad = 1.0/sqrt(pow(r(),-2.0/3.0) - 1.0); //radius in units of a_plummer
    struct vector u = randomvector(rad);
    p.x = u.x;
    p.y = u.y;
    p.z = u.z;
    // Initialize its velocity extracting from Plummer distribution 
    double x = 0.0;
    double y = 0.1;
    while (y > pow(x*x*(1.0-x*x), 3.5))
    {
        x = r();
        y = 0.1*r();
    }
    double vel = x * sqrt(2.0) * pow(1.0 + rad*rad, -0.25);
    u = randomvector(vel);
    p.vx = u.x;
    p.vy = u.y;
    p.vz = u.z;
    return(p);
}

void printparticle(struct particle p, long step)
{
    printf("%ld %ld %f %f %f %f %f %f\n", step, p.ID, p.x, p.y, p.z, p.vx, p.vy, p.vz);
}

int main(int argc, char *argv[])
{
    struct particle p;
    long i, j, k, Nnoout, Nparticles = 10000;
    long Nsteps_before_shock = 30; //useless, in principle - this does not 'relax'
    long Nsteps_after_shock = 30; //the crossing time of M67 is 1.7 Myr, it hit the disk 40 Myr ago
    double stepsize = 0.005;
    double GM_over_a2_plummer = 1;
    double Vhat = 20.0; //Vhat is V / sqrt(GM/a) where V is the velocity with which M67 crossed the disk and sqrt(GM/a) is the circular velocity at Plummer radius a
    double massratio = 0.4; //massratio is Sigma pi a^2 / M where Sigma is the surface mass density of the disk, M is the mass of M67 and a its Plummer radius 
    // a is of order 3 parsec, M is 2000 Msun, so sqrt(GM/a) is ~ 1.5 km/s and V ~ 30 km/s so Vhat = 20
    // Sigma is 30 Msun/pc^2 so Sigma pi a^2 = 30*pi*9 ~ 850 and Sigma pi a^2 / M = 850/2000 ~ 0.4
    double Rhhat = 4.0; //Ratio of Hill radius to Plummer radius a
    double intensity = 4*massratio/Vhat; //it should be about 0.08 for M67

    if (argc > 1)
    {
        Nparticles = atoi(argv[1]);
        printf("# Number of particles: %ld \n", Nparticles);
        if (argc > 2)
        {
            stepsize = atof(argv[2]);
            printf("# Stepsize: %f \n", stepsize);
            if (argc > 3)
            {
                intensity = atof(argv[3]);
                printf("# Disk shocking intensity: %f \n", intensity);
                if (argc > 4)
                {
                    Rhhat = atof(argv[4]);
                    printf("# Hills radius / Plummer radius: %f \n", Rhhat);
                }
            }
        }
    }

    Nnoout = (long)(1.0/stepsize); //if Nnoout*stepsize = 1 the time reported in snapshot is in units of crossing time
    double Omega = pow(Rhhat, -1.5)/sqrt(3.0);
    srand(37);

    printf("time ID x y z vx vy vz\n");
    for(j = 0; j < Nparticles; j++)
    {
        p = newparticle(j);
        printparticle(p, 0);
        for(i = 0; i < Nsteps_before_shock; i++)
        {
            for(k = 0; k < Nnoout; k++)
            {
                p = onestep(p, GM_over_a2_plummer, stepsize, 0.0);  
            }
            printparticle(p, i+1);
        }
        p = diskShock(p, intensity);
        printparticle(p, Nsteps_before_shock+1);
        for(i = 0; i < Nsteps_after_shock; i++)
        {
            for(k = 0; k < Nnoout; k++)
            {
                p = onestep(p, GM_over_a2_plummer, stepsize, Omega);
            }  
            printparticle(p, Nsteps_before_shock + i + 2);
        }
    }
    return(0);
}