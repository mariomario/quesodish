# QUESODISH (QUinn hill-Equation SOlver + DIsk SHocking)


QUESODISH is a C implementation of the Quinn 2010 second-order symplectic integrator for the Hill equation, with the addition of an impulsive disk shocking prescription.
It can be used to model the evolution of star clusters orbiting a point-mass galaxy.
