#!/bin/bash
for int in 0.0 0.01 0.02 0.04 0.08 0.16
do
    ./QD 10000 0.001 $int 5 > prova
    rm -f .R*
    R CMD BATCH movie.R
    mv Rplots.pdf "shock$int.pdf"
done
